#!/usr/bin/env python2.7
# *-* coding: utf-8 *-*

import rospy
from std_msgs.msg import Float64
from Controle import *


class wheel:

    def __init__(self, kp, ki, kd, torque_topic, angular_vel_topic,set_point_topic,set_point, raio=0.1):
        #cria parametros para receber os valores do servidor
        self.wheel_kp = rospy.get_param(kp)
        self.wheel_ki = rospy.get_param(ki)
        self.wheel_kd = rospy.get_param(kd)
        #inicializa uma variavel do tipo Float64
        self.new_msg = Float64()
        
        #inicializa o setpoint
        self.set_point = set_point
        
        #inicializa a mensagem do callback 
        self.msg = 0
        
        #inscreve no topico de setpoint da roda desejada 
        self.sub_new = rospy.Subscriber(
            set_point_topic, Float64, self.funcao_callback_new)
        
        #publica no topico de torque da roda desejada 
        self.pub = rospy.Publisher(torque_topic, Float64, queue_size=10)
        
        #inscreve no topico de velocidade angular da roda desejada
        self.sub = rospy.Subscriber(
            angular_vel_topic, Float64, self.funcao_callback)
        
        #instancia um objeto da classe controle
        self.v = Controle(self.wheel_kp,
                          self.wheel_kd, self.wheel_ki, 0.1)
    
    #funcao callback do subscriber da velocidade angular
    def funcao_callback(self, msg):
        #dado recebebido pelo subscriber
        data = msg.data
        
        v_angular = self.v.control(self.set_point, data)
        #atualiza o v_angular por new_msg
        self.new_msg.data = v_angular
        #publica esse new_msg
        self.pub.publish(self.new_msg)

    def funcao_callback_new(self, msg1):
        self.set_point = msg1.data


def main():
    kpl = "/left_wheel_kp"
    kil = "/left_wheel_ki"
    kdl = "/left_wheel_kd"

    kpr = "/right_wheel_kp"
    kir = "/right_wheel_ki"
    kdr = "/right_wheel_kd"

    rospy.init_node('Controlador')
    ob_l = wheel(kpl, kil, kdl, "/left_wheel_link/torque_input",
                 "/left_wheel_link/angular_vel","/Set_Point_left",0.05)
    ob_r = wheel(kpr, kir, kdr, "/right_wheel_link/torque_input",
                 "/right_wheel_link/angular_vel","/Set_Point_Right",-0.05)
    rospy.spin()

main()
