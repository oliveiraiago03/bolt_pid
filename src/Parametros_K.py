#!/usr/bin/env python2.7
# *-* coding: utf-8 *-*

#Parametros de controle
import rospy
from std_msgs.msg import *


rospy.set_param("left_wheel_kd",0.0)
rospy.set_param("left_wheel_kp",1.0)
rospy.set_param("left_wheel_ki",1.0)


rospy.set_param("right_wheel_kd",0.0)
rospy.set_param("right_wheel_kp",1.0)
rospy.set_param("right_wheel_ki",1.0)


left_wheel_kd = rospy.get_param("left_wheel_kd")
left_wheel_ki = rospy.get_param("left_wheel_ki")
left_wheel_kp = rospy.get_param("left_wheel_kp")


right_wheel_kd = rospy.get_param("right_wheel_kd")
right_wheel_ki = rospy.get_param("right_wheel_ki")
right_wheel_kp = rospy.get_param("right_wheel_kp")

