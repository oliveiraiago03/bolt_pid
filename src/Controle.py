#!/usr/bin/env python2.7
# *-* coding: utf-8 *-*
from __future__ import division
import time
class Controle:
    def __init__(self,kp,kd,ki,raio):

        self.kp = kp
        self.kd = kd
        self.ki = ki
	self.raio = raio

        self.previous_error = 0
        self.integral = 0
        self.previous_time = 0

    def control(self,setpoint,entrada):
        if self.previous_time == 0:
            self.previous_time = time.time()
            return

        now = time.time()
        delta_time = now - self.previous_time
        print(delta_time)

        error = setpoint - entrada*self.raio

        self.integral += error*delta_time
        derivative = (error - self.previous_error)/(delta_time)

        p = self.kp*error 
        i = self.ki*self.integral 
        d = self.kd*derivative
        s = p+i+d
        print('esse e o p',p)
        print('esse e o d',d)
        print('esse e o i',i)
        v_ang = s

        self.previous_error+=error

        self.previous_time = now
        self.previous_error = error
        
        return v_ang
if __name__ == "__main__":
    import sys
    
